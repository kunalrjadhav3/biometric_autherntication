# Description

This project is used to showcase client server biometric authentication in Android.

  - Project uses FingerprintManagerCompat for biometric authentication on android devices targeted between API level 23 (Marshmellow) to API level 26 (Orieo)
  - Project uses BiometricPrompt for biometric authentication on android devices targeted with API level 28 (Pie) and above
  - Client-server authentication keys are generated using secp256r1 and stored securely in AndroidKeyStore  
  - ECDSA assymetric cryptographic algorithm is used for signing at the client (hashed with SHA256) and verifying signature at the server